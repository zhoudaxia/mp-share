1. methods 与 自定义方法 
自定义方法才是vue中的methods里的方法，
methods的应该在事件中使用，不清楚分开的意义与限制。
2. 组件id 不能驼峰
3. twoWay sync
4. this.$apply
5. 事件 $emit $invoke $broadcast
6. 绑定事件修饰符 .user .stop .default
7. mixins也要继承
8. wxs支持
9. **intercepter**
10. **promise类型api**
11. **单文件**
12. 需注意的是，在异步函数中更新数据的时候，必须手动调用$apply方法，才会触发脏数据检查流程的运行
13. 可以使用setData
14. 支持类似import模板，将模板当作组件
15. repeat的bug

坑：
1. 同名组件是单例
2. 事件传播模式,加上.user修饰符和vue差不多
